/* Функция вызывается при клике на игровой элемент
    @param: event - объект события, в котором хранится игровой элемент */
const eventByClickByElement = (e) => {
    const { target } = e
    const element = target.parentNode.parentNode
    const type = element.getAttribute('type')
    const size = element.getAttribute('size')
    element.classList.add('hidden')
    store.user.setPoints(store.user.getPoints() + gameOptions[type][size])
    switch (type){
        case 'fruit':
            store.user.setFruitsCount(store.user.getFruitsCount() + 1)
            break
        case 'trash':
            store.user.setTrashCount(store.user.getTrashCount() + 1)
            break
    }
    if (store.user.getPoints() > countToLive) {
        store.user.setLives(store.user.getLives() + 1)
        countToLive += 100
    }
    document.querySelector('user-scores').updateCount()
    document.querySelector('user-scores').updatePoints()
    document.querySelector('user-scores').updateLives()
}