/* Настройки получаемого количества очков при клике на конкретный элемент */
const gameOptions = {
    fruit: {
        small: 30,
        medium: 20,
        large: 10
    },
    trash: {
        all: -30
    }
}
