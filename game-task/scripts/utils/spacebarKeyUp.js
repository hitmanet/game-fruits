/* Функция, инициализирующая паузу или продолжение игры при клике на пробел
   @param: e - объект события keyup*/
const spacebarKeyUp = (e) => {
    if (e.keyCode == 32) {
        mutations.setPaused(!store.paused)
        if (store.paused){
            $('game-element').pause()
            document.querySelector('#play-btn').innerHTML = 'Play'
            for (const item of document.querySelectorAll('.fruit, .trash')){
                item.removeEventListener('click', eventByClickByElement)
            }
        } else {
            $('game-element').resume()
            document.querySelector('#play-btn').innerHTML = 'Pause'
            for (const item of document.querySelectorAll('.fruit, .trash')){
                item.addEventListener('click', eventByClickByElement)
            }
        }
    }
}