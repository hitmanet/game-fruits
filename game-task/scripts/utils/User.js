/* Класс пользователя (игрока)
   @param: username - имя пользователя*/
class User {
    constructor(username){
        this.username = username
        this.points = 0
        this.lives = 20
        this.TrashCount = 0
        this.fruitsCount = 0
    }

    setTrashCount(count){
        this.TrashCount = count
    }

    getTrashCount() {
        return this.TrashCount
    }

    setFruitsCount(count){
        this.fruitsCount = count
    }

    getFruitsCount(){
        return this.fruitsCount
    }

    getPoints (){
        return this.points
    }

    setPoints (points) {
        this.points = points
    }

    getLives() {
        return this.lives
    }

    setLives (lives) {
        this.lives = lives
    }
}