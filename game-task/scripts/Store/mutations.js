/* Мутации, изменяющие глобальное хранилище приложения */

const mutations = {
    setUser: (user) => {
        store.user = user
    },
    setPaused: (paused) => {
        store.paused = paused
    },
    setTestedMode: (tested) => {
        store.tested = tested
    }
}