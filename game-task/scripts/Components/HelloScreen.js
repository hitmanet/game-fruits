class HelloScreen extends HTMLElement {
    constructor(){
        super()

        this.innerHTML = `
        <form>
            <input type="text" placeholder="Enter your name">
            <input type="submit" disabled value="Play game">
        </form>
        `

        /* Проверяет, что поле с именем не пустое и в зависимости от этого разрешает вход */
        this.querySelector('input[type = "text"]').addEventListener('keyup', () => {
            if (this.querySelector('input[type="text"]').value.length === 0){
                this.querySelector('input[type = "submit"]').setAttribute('disabled', 'disabled')
            } else {
                this.querySelector('input[type = "submit"]').removeAttribute('disabled')
            }
        })

        /* Проверяет, что ввведено имя пользователя и проверяет, является ли пользователем тестером */
        this.querySelector('input[type = "submit"]').addEventListener('click', (e) =>{
            e.preventDefault()
            if (this.querySelector('input[type="text"]').value.length > 0){
                const user = new User(this.querySelector('input[type="text"]').value)
                mutations.setUser(user)
                fsm.performTransition('playGame')
            } else {
                alert('Введите имя пользователя')
            }
            if (this.querySelector('input[type="text').value === 'tester'){
                mutations.setTestedMode(true)
            }
        })
    }
}

customElements.define('hello-screen', HelloScreen)