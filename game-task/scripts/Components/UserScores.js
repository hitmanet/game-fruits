class UserScores extends HTMLElement {
    constructor() {
        super()
        this.innerHTML = `<div id="user-stats">
                                <ul>
                                    <li id="username">${store.user.username}</li>
                                    <li><div id="timer">1:20</div></li>
                                </ul>
                                <ul>
                                    <li id="lives">Your lives: ${store.user.getLives()}</li>
                                    <li><span class="user-points">${store.user.getPoints()}</span></li>
                                    <li><button id="play-btn">Pause</button></li>
                                </ul>
                                <ul id="click-count">
                                    <li id="fruits-count">Fruits: ${store.user.getFruitsCount()}</li>
                                    <li id="Trash-count">Trash: ${store.user.getTrashCount()}</li>
                                </ul>
                            </div>
                           `
        /* Устанавливает или снимает меню паузы в зависимости от глобального хранилища */
        this.querySelector('#play-btn').onclick = (e) => {
            mutations.setPaused(!store.paused)
            const items = document.querySelectorAll('.fruit, .trash')
            if (!store.paused){
                e.target.innerHTML = 'Pause'
                for (const item of items){
                    item.addEventListener('click', eventByClickByElement)
                }
                $('game-element').resume()
                this.startTimer()
            } else {
                e.target.innerHTML = 'Play'
                for (const item of items){
                    item.removeEventListener('click', eventByClickByElement)
                }
                $('game-element').pause()
            }
        }

        this.startTimer = this.startTimer.bind(this)
        this.startTimer()

        window.addEventListener('keyup', spacebarKeyUp)
    }

    /* Обновляет поле с очками пользователя */
    updatePoints(){
        this.querySelector('.user-points').innerHTML = store.user.getPoints()
    }

    /* Обновляет количество нажатых элементов пользователя */
    updateCount() {
        this.querySelector('#fruits-count').innerHTML = `Fruits: ${store.user.getFruitsCount()}`
        this.querySelector('#Trash-count').innerHTML = `Trash: ${store.user.getTrashCount()}`
    }

    /* Обновляет количество жизней пользователя */
    updateLives(){
        this.querySelector('#lives').innerHTML = `Your lives: ${store.user.getLives()}`
    }

    /* Инициализирует старт таймера */
    startTimer() {
        let minute, second, secondWithCheck
        if (store.tested){
            this.querySelector('#timer').innerHTML = 'Tested Mode'
            this.querySelector('#username').classList.add('tested-mode')
        } else {
            if (!store.paused){
                const currentTime = this.querySelector('#timer').innerHTML
                const timeArr = currentTime.split(':')
                minute = timeArr[0]
                second = timeArr[1]
                secondWithCheck = checkSecond(second - 1)
                if (secondWithCheck == 59) {
                    minute--
                }
                this.querySelector('#timer').innerHTML = `${minute} : ${secondWithCheck}`
            }
            const timer = setTimeout(this.startTimer, 1000)
            checkTime(minute, secondWithCheck, timer)
        }
    }
}


/* Функция реализует логику поведения секунд в таймере
    @param: second - текущее количество секунд */
function checkSecond(second) {
    if (second < 10 && second >= 0) {
        second = `0${second}`
    }
    if (second < 0) {
        second = '59'
    }
    return second
}

/* Функция проверяет окончание таймера и заканчивает игру в этом случае
    @param: minute - текущие минуты
    @param: second - текущие секунды
    @param: timer - функция setInterval, которая останавливается при условии окончания таймера */
function checkTime(minute, second, timer) {
    if (minute == 0 && second == 0){
        clearTimeout(timer)
        mutations.setPaused(true)
        endGame()
        window.removeEventListener('keyup', spacebarKeyUp)
        fsm.performTransition('endGame')
    }
}

customElements.define('user-scores', UserScores)