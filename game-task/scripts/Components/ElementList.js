
const arraySizes = ['small', 'medium', 'large']
const typeSizes = ['trash', 'fruit']
class ElementList extends HTMLElement {
    constructor(){
        super()
        this.elementsArray = []
        this.innerHTML = `<div class="element-list"></div>`
    }

    connectedCallback(){
        this.createNewElements()
    }


    /* Инициализирует стартовый рендер массива из игровых элементов
       @param: elementsArray - массив игровых элементов */
    render(elementsArray){
        for (const el of elementsArray){
            this.querySelector('.element-list').append(el)
            this.animated(el)
        }
    }


    /* Создает новый элемент на основании уже существующего после его падения/нажатия
       @param: el - элемент, на основании которого создается новый */
    createNewElement(el) {
        el.classList.remove('hidden')
        el.style.top = 0
        el.style.left = 0
        el.style.right = 0
        if (document.querySelectorAll('.trash').length >= 5){
            el.setAttribute('type', 'fruit')
            el.setAttribute('size',  arraySizes[randomInteger(0,2)])
        } else if (document.querySelectorAll('.fruit') >= 5){
            el.setAttribute('type', 'trash')
            el.setAttribute('size', 'all')
        } else {
            const type = typeSizes[randomInteger(0, 1)]
            const size = type === 'trash' ? 'all' : arraySizes[randomInteger(0, 2)]
            el.setAttribute('type', type)
            el.setAttribute('size', size)
        }
        el.children[0].children[0].className = `${el.getAttribute('type')} ${el.getAttribute('size')}`
        this.animated(el)
    }


    /* Реализует анимацию и поведение элемента после нее.
        @param - элемент, для которого запускается анимация*/
    animated(el) {
        $(el).animate({ top: '+=100vh', left: `+=${randomInteger(0, 10)}%`, right: `+=${randomInteger(5, 15)}%` }, {
                duration: randomInteger(5000, 8000),
                complete: () => {
                    if (!store.paused){
                        if (!(el.classList.contains('hidden'))) {
                            el.classList.add('hidden')
                            if (el.getAttribute('type') === 'fruit'){
                                store.user.setPoints(store.user.getPoints() - 10)
                                if (!store.tested){
                                    store.user.setLives(store.user.getLives() - 1)
                                }
                            }
                            document.querySelector('user-scores').updatePoints()
                            document.querySelector('user-scores').updateLives()
                            if ((store.user.getLives() === 0 || store.user.getPoints() >= 500) && !store.tested ){
                                mutations.setPaused(true)
                                endGame()
                                window.removeEventListener('keyup', spacebarKeyUp)
                                fsm.performTransition('endGame')
                            }
                        }
                        this.createNewElement(el)
                    }
                }
            }
        )
    }

    /* Создает массив элементов и вызывает первоначальный рендер */
    createNewElements(){
        const elementsArray = []
        const randForFruits = 5
        const randForTrash = 5
        for (let i = 0; i< randForFruits; i+=1){
            const fruit = document.createElement('div')
            fruit.innerHTML = `<game-element type="fruit" size="${arraySizes[randomInteger(0, 2)]}"/>`
            elementsArray.push(fruit.firstChild)
        }
        for (let j = 0; j < randForTrash; j+=1) {
            const trash = document.createElement('div')
            trash.innerHTML = `<game-element type="trash" size="all"/>`
            elementsArray.push(trash.firstChild)
        }
        elementsArray.sort(compareRandom)
        this.render(elementsArray)
    }
}

const randomInteger = (min, max) => {
    let rand = min + Math.random() * (max + 1 - min)
    rand = Math.floor(rand)
    return rand
}

const compareRandom = (a, b) => Math.random() - 0.5

customElements.define('element-list', ElementList)