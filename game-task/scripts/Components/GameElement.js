let countToLive = 100

class GameElement extends HTMLElement {
    constructor(){
        super()

        this.innerHTML = `
            <div class="element-wrapper">
                <div class="${this.getAttribute('type')} ${this.getAttribute('size')}"></div>
            </div>
        `
        /* Навешивается событие клика на элемент, в котором реализована логика начисления очков в зависимости от типа и размера элемента */
        this.querySelectorAll('div')[1].addEventListener('click', eventByClickByElement)
    }
}

customElements.define('game-element', GameElement)