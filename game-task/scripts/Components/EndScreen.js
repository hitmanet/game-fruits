class EndScreen extends HTMLElement {
    constructor(){
        super()
        this.render()
    }
    /* Функция выполняет построении таблицы лидеров в зависимости от результата пользователя */
    render(){
        const users = JSON.parse(localStorage.getItem('users'))
        users.users.sort((a,b) => b.points - a.points)
        users.users.map((value, index) => {
            value.place = index + 1
            value.isCurrentUser = value.username === store.user.username && value.points === store.user.points
        })
        const top = users.users.slice(0, 9)
        let currentUser = {}
        let currentUserHere = false
        let tableHtml = ''
        for (const userIndex in users.users) {
            const user = users.users[userIndex]
            if (user.isCurrentUser) {
                currentUserHere = true
                tableHtml += `<tr class="isCurrentUser"><td>${user.place}</td><td>${user.username}</td><td>${user.points}</td></tr>`
            } else {
                tableHtml += `<tr><td>${user.place}</td><td>${user.username}</td><td>${user.points}</td></tr>`
            }
            if (userIndex >= 8) {
                if (!currentUserHere){
                    currentUser = users.users.find((value) => value.isCurrentUser)
                    tableHtml += `<tr class="isCurrentUser"><td>${currentUser.place}</td><td>${currentUser.username}</td><td>${currentUser.points}</td></tr>`
                } else {
                    tableHtml += `<tr><td>${users.users[9].place}</td><td>${users.users[9].username}</td><td>${users.users[9].points}</td></tr>`
                }
                break
            }
        }
        this.innerHTML = `
            <h3 class="white">Вы кликнули на ${store.user.getFruitsCount()} фрукта и на ${store.user.getTrashCount()} мусора</h3>
            <table>
                <thead>
                    <tr>
                        <th>Место</th>
                        <th>Имя пользователя</th>
                        <th>Очки</th>
                    </tr>
                </thead>
                <tbody>
                    ${tableHtml}
                </tbody>
            </table>`
    }
}

customElements.define('end-screen', EndScreen)

