/* Возможные переходы для состояний */ 
const transitions = {
    [states.START_SCREEN] : {
        playGame: () => {
            if(store.user !== null) {
               return states.PLAYABLE
            }
        }
    },
    [states.PLAYABLE] : {
        endGame: () => states.FINAL_SCREEN
    }
}

function* transition(){
    const user = store.user
    yield { newState: states.PLAYABLE, user }
    yield { newState: states.FINAL_SCREEN, points: user.points }
    yield { newState: states.START_SCREEN }
}