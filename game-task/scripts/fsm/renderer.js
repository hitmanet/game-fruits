/* Реализация рендеринга игрового поля в зависимости от состояния конечного автомата
    @param: state - состояник конечного автомата
    return domElement - элемент, который рендерится на поле */ 
const render = (state, payload) => {
    switch(state){
        case states.START_SCREEN:
            return document.createElement('hello-screen')
        case states.PLAYABLE:
            const userScore = document.createElement('div')
            userScore.innerHTML = '<user-scores></user-scores>'
            const elementList = document.createElement('div')
            elementList.innerHTML = '<element-list></element-list>'
            const div = document.createElement('div')
            div.appendChild(userScore.firstChild)
            div.appendChild(elementList.firstChild)
            const startBlock = document.createElement('div')
            startBlock.id = 'start-block'
            const endBlock = document.createElement('div')
            endBlock.id = 'end-block'
            div.appendChild(startBlock)
            div.appendChild(endBlock)
            return div
        case states.FINAL_SCREEN:
            const end = document.createElement('div')
            end.id = 'end'
            end.innerHTML = '<end-screen></end-screen>'
            return end
        default: return ''
    }
}