
/* Класс конечного автомата состояний
    @param: initial - начальное состояние автомата 
    @param: states - возможные состояния приложения
    @param: transitions - функции, изменяющие состояния
    @data: data - данные, связанные с состояния*/
class StateMachine {
    constructor({ initial, states, transitions, data = null }) {
        this.transitions = transitions
        this.states = states
        this.state = initial
        this.data = data
    }

    /* returns: this.state - текущее состояние приложения */
    stateOf() {
        return this.state
    }

    /*
        @param: event - событие, на которое подписывается автомат
        @param: callback - функция для апдейта состояния
    */
    subscribe(event, callback) {
        if (event === 'update') this._onUpdate = callback || null
    }

    /* Функция обновляет текущее состояние
       @param: newState - новое состояние
       @param: data - данные, связанные с состоянием*/
    _updateState(newState, data = null) {
        this.state = newState
        this.data = data
        this._onUpdate
        && this._onUpdate(newState, data)
    }

    /* Получает и вызывает функцию перехода состояния
        @param: transitionName - название перехода состояния*/
    performTransition(transitionName) {
        const possibleTransitions = this.transitions[this.state]
        const transition = possibleTransitions[transitionName]
        if (!transition) return
        this._updateState(transition())
    }


}