/* Возможные состояния приложения */
const states = {
    START_SCREEN: 'start',
    PLAYABLE: 'play',
    FINAL_SCREEN: 'final'
}