/* Удаление всех детей у элемента 
    param: domNode - элемент, у которого удаляются дети*/
const removeAllChildren = (domNode) => {
    while (domNode.firstChild) {
        domNode.removeChild(domNode.firstChild)
    }
}

/* Рендер приложения в зависимости от состояния конечного автомата
    @param: state - состояния автомата
    @param: data - данные, необходимые для рендеринга*/
const renderApp = (state, data) => {
    const html = render(state, data)
    removeAllChildren(document.getElementById('game-board'))
    document.getElementById('game-board').appendChild(html)
}


/* Инициализация конечного автомата, инициализация рендеринга, подписывание на изменение состояния и перерендинг */

const fsm = new StateMachine({
    states,
    transitions,
    initial: states.START_SCREEN
})

renderApp(states.START_SCREEN)

fsm.subscribe('update', (state, data) => {
    renderApp(state, data)
})




